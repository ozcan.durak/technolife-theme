<?php

return [
    'help'      => 'Yardım',
    'search'    => 'Arama',
    'logout'    => 'Çıkış Yap',
    'view_site' => 'Siteyi Görüntüle',
    'title'     => 'Kontrol Paneli',
];
