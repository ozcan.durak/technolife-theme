<?php

return [
    'title'                     => 'Yardımcı Kaynaklar',
    'description'               => 'Bu yararlı kaynaklarla yardım bulun ve özellikleri keşfedin.',
    'documentation_link'        => 'Technolife Dokumanları',
    'documentation_description' => 'Technolife, eklentiler geliştirme ve eklentileri kullanma ile ilgili belgeleri bulun.',
    'slack_link'                => 'Slack Takımı',
    'slack_description'         => 'Diğer Technolife kullanıcılarını ve geliştiricilerini bulun ve iletişim kurun.',
    'forum_link'                => 'Tartışma forumu',
    'forum_description'         => 'Technolife\'ı kullanma ve geliştirme ile ilgili cevapları bulun ve sorular gönderin.',
    'addons_link'               => 'Kullanılabilir Eklentiler',
    'addons_description'        => 'Technolife için kullanılabilecek eklentileri keşfedin.',
];
