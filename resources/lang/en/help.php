<?php

return [
    'title'                     => 'Helpful Resources',
    'description'               => 'Find help and discover features with these helpful resources.',
    'documentation_link'        => 'OpenClassify Documentation',
    'documentation_description' => 'Find documentation for Technolife, developing addons, and using addons too.',
    'slack_link'                => 'Slack Team',
    'slack_description'         => 'Find and communicate with other Technolife users and developers.',
    'forum_link'                => 'Discussion Forum',
    'forum_description'         => 'Find answers and post questions about using and developing with Technolife.',
    'addons_link'               => 'Available Addons',
    'addons_description'        => 'Discover addons available for Technolife.',
];
