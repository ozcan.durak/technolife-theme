<?php

return [
    'title'       => 'Technolife Admin',
    'name'        => 'Technolife Admin Theme',
    'description' => 'The official admin theme for Technolife.',
];
