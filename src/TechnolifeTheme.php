<?php namespace Visiosoft\TechnolifeTheme;

use Anomaly\Streams\Platform\Addon\Theme\Theme;

/**
 * Class TechnolifeTheme
 *
 * @link   http://openclassify.com/
 * @author OpenClassify, Inc. <support@openclassify.com>
 * @author Vedat Akdogan <vedat@openclassify.com>
 */
class TechnolifeTheme extends Theme
{

    /**
     * This is an admin theme.
     *
     * @var bool
     */
    protected $admin = true;
}
